# Telling Time on a Torus

Inspired by a [PBS Infinite Series video](https://www.youtube.com/watch?v=KZT5hrYOERs), 
I attempt to "find all valid [12 hr] clock configurations for which swapping the 
hours and minutes hands results in a valid configuration."


