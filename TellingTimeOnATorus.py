### Prompt:
# https://www.youtube.com/watch?v=KZT5hrYOERs&feature=youtu.be
'''
"Find all valid [12 hr] clock configurations for which swapping the hours and minutes
hands results in a valid configuration"

Report times in hh:mm:ss nn/dd, e.g. 01:05:27 3/11
Report all and only correct times
'''


